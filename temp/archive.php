<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Audiotext
 */

get_header(); ?>

<div class="pg pg-blog single">

	<div class="cabecalhoBlog">
		<div class="areaCabecalho">
			<div class="row">
				<div class="col-sm-6">
					<nav class="nav">
						<span>Você está aqui:</span>
						<?php
							$i = 0;
							if ( have_posts() ) : while ( have_posts() ) : the_post();if ($i < 1):
								global $post;
								$category_detail=get_the_category($post->ID);//$post->ID
						?>
						<a href="<?php echo home_url('/'); ?>"><i class="fa fa-home" aria-hidden="true"></i> Home /</a>
						<a href="<?php echo home_url('/blog/'); ?>"><i class="fa fa-home" aria-hidden="true"></i>Blog /</a>
						<?php foreach($category_detail as $cd):?>
						
						<a href="<?php echo get_category_link($cd->cat_ID) ?>"><?php echo  $cd->cat_name; ?> /</a>
						
						<?php  endforeach;$i++;endif; endwhile; endif; ?>
					</nav>
				</div>
				<div class="col-sm-6">
					<div class="titulo">
						<?php $i = 0; if ( have_posts() ) : while ( have_posts() ) : the_post(); if ($i < 1):?>
						<h2><?php echo get_the_title() ?></h2>
						<?php $i++;endif; endwhile; endif; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container">

		<div class="col-sm-8">
			<div class="areaPosts">

				<ul>
					<?php
						if ( have_posts() ) : while ( have_posts() ) : the_post();
							global $post;
							$imagemDestacada = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), array(300,300) );
							$imagemDestacada = $imagemDestacada[0];
							$category_detail=get_the_category($post->ID);//$post->ID
					?>
					<li>
						<figure style="background:url(<?php echo $imagemDestacada ?>)">
							<div class="links">
								<a href="<?php echo $imagemDestacada ?>" id="fancy" rel="gallery1" >
									<i class="fa fa-picture-o" aria-hidden="true"></i>
								</a>	
								<a href="<?php echo get_permalink() ?>">
									<i class="fa fa-external-link" aria-hidden="true"></i>
								</a>
								
							</div>
						</figure>
						<a href="<?php echo get_permalink() ?>">
							<h2><?php echo get_the_title() ?></h2>
							<?php foreach($category_detail as $cd):?>
							<h3><?php echo  $cd->cat_name; ?>,</h3>
							<?php endforeach; ?>
							<p><?php customExcerpt(500); ?></p>

							<span class="link">Ler Mais</span>
						</a>
					</li>
					<?php endwhile; endif; ?>
				</ul>

			</div>
		</div>

		<?php get_sidebar(); ?>

	</div>	

	<?php if ($configuracao['paginas_transcricao_seja_um_texter_hidden'] != "Esconder"):?>
	<div class="areaSejaumtexter">
		<p><?php echo $configuracao['opt_inicial_seja_um_texter'] ?></p>
		<a href="<?php echo $configuracao['opt_inicial_seja_um_texter_btn_link'] ?>"><?php echo $configuracao['opt_inicial_seja_um_texter_btn'] ?></a>
	</div>
	<?php endif; ?>
</div>

<?php
get_footer();
