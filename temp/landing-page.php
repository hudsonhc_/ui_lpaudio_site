<?php
/**
* Template Name: Landing Page
* Description:
*
* @package audiotext
*/
global $post;
global $configuracao;
get_header(); ?>
	<!-- LANDING PAGE -->
	<div class="pg pg-landingPage">
		<!-- SESSÃO INICIAL -->
		<section class="sessaoInicial">
			<div class="container">
				<!-- TITULO DA SESSAO -->
				<h1 class="tituloSessao"><?php echo $configuracao['landing_page_destaque_titulo'];?></h1>
			</div>
			<!-- BANNER DESTAQUE -->
			<div class="bannerDestaqueLP" style="background: url(<?php echo $configuracao['landing_page_destaque_imagem_banner']['url'];?>);">
				<div class="container">
					<div class="conteudoBanner">
						<!-- TITULO DO DESTAQUE -->
						<h2 class="tituloDestaqueLP"><?php echo $configuracao['landing_page_destaque_texto_banner'] ?></strong></h2>
						<!-- BOTAO ENVIAR ARQUIVOS -->
						<a href="<?php echo $configuracao['landing_page_destaque_link_botao_banner'] ?>" class="botaoEnviarArquivos" target="_blank">Enviar arquivos</a>
					</div>
				</div>
			</div>
			
			<!-- SESSAO SOBRE TRANSCRICAO AUTOMATICA -->
			<section class="sobreTranscricaoAutomatica">
				<div class="container">
					<!-- TITULO -->
					<h2><?php echo $configuracao['lp_transcricao_automatica_titulo'] ?></h2>
					<!-- DESCRICAO -->
					<article>
						<p><?php echo $configuracao['lp_transcricao_automatica_descricao'] ?></p>
					</article>


					<h4><?php echo $configuracao['lp_transcricao_automatica_subtitulo'] ?></h4>
					<div class="botoesSobre">
						<a href="#analiseLigacoes" class="analiseDeLigacoes ancoraLandingPage"><?php echo $configuracao['lp_transcricao_automatica_botao_1'] ?></a>
						<a href="#searchableContent" class="conteudoBuscavel ancoraLandingPage"><?php echo $configuracao['lp_transcricao_automatica_botao_2'] ?></a>
					</div>
				</div>
			</section>
		</section>

		<!-- SESSÃO SOBRE TECNOLOGIA -->
		<section class="sobreTecnologia">
			<div class="container">
				<!-- TÍTULO DA SESSAO -->
				<h2>Por que temos a melhor tecnologiade reconhecimento de voz?</h2>
				
				<!-- LISTA DE TÓPICOS -->
				<ul class="listaDeTopicos">
					<?php 
						// LOOP PLATAFORMA PORTFOLIO
						$postTecnologias = new WP_Query(array(
								'post_type'     => 'tecnologias',
								'posts_per_page'   => 4,
							)
						);
						while($postTecnologias->have_posts()) : 
							$postTecnologias->the_post();
							$imagemTecnologia = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
							$imagemTecnologia = $imagemTecnologia[0];
					?>
					<!-- TÓPICO -->
					<li class="topico">
						<article>
							<figure>
								<img src="<?php echo $imagemTecnologia; ?>" alt="<?php echo get_the_title(); ?>">
							</figure>
							<p class="descricaoTopico"><?php echo the_content(); ?></p>
						</article>
					</li>
					<?php endwhile; wp_reset_query(); ?>
			</div>
		</section>
		
		<!-- SESSÃO VOICE ANALYTICS -->
		<section class="voiceAnalytics" id="analiseLigacoes">
			<div class="container">
				<article>
					<h2><?php echo $configuracao['lp_voice_analytics_titulo'] ?></h2>

					<!-- CONTEUDO -->
					<?php echo $configuracao['lp_voice_analytics_conteudo'] ?>

					<h4><?php echo $configuracao['lp_voice_analytics_subtitulo'] ?></h4>
					<a href="<?php echo $configuracao['lp_voice_analytics_link_botao'] ?>" target="_blank" class="botaoEnviarArquivos">Enviar arquivos</a>
				</article>
			</div>
		</section>

		<!-- SESSÃO SEARCHABLES CONTENT -->
		<section class="searchableContent" id="searchableContent">
			<div class="container">
				<article>
					<h2><?php echo $configuracao['lp_searchable_content_titulo'] ?></h2>

					<!-- CONTEUDO -->
					<?php echo $configuracao['lp_searchable_content_conteudo'] ?>

					<h4><?php echo $configuracao['lp_searchable_content_subtitulo'] ?></h4>
					<a href="<?php echo $configuracao['lp_searchable_content_link_botao'] ?>" target="_blank" class="botaoEnviarArquivos">Enviar arquivos</a>
				</article>
			</div>
		</section>
	</div>
	<div class="pg pg-inicial">
		<div class="areaSejaumtexter">
	        <h2>
	            <?php echo $configuracao['opt_inicial_seja_um_texter'] ?>
	        </h2>
	       <span class="abrirModalEntreParaOTime">
	            <?php echo $configuracao['opt_inicial_seja_um_texter_btn'] ?>
	        </span>
	    </div>
	</div>

<?php get_footer(); ?>