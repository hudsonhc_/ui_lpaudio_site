<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Audiotext
 */
$idVideo = rwmb_meta("Audiotext_id_video_LP");
get_header(); ?>
<!-- SINGLE VIDEO -->
	<div class="pg pg-single-video">
		<div class="container">
			<h1 class="tituloVideo"><?php echo get_the_title(); ?></h1>

			<div class="iframeVideo">
				<iframe width="1280" height="720" src="https://www.youtube.com/embed/<?php echo $idVideo; ?>?ecver=1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
			</div>

			<div class="conteudoVideo">
				<?php echo the_content(); ?>
			</div>
		</div>
	</div>
	<div class="pg pg-inicial">
		<div class="areaSejaumtexter">
	        <h2>
	            <?php echo $configuracao['opt_inicial_seja_um_texter'] ?>
	        </h2>
	       <span class="abrirModalEntreParaOTime">
	            <?php echo $configuracao['opt_inicial_seja_um_texter_btn'] ?>
	        </span>
	    </div>
	</div>

<?php get_footer();?>