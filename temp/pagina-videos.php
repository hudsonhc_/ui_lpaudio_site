<?php
/**
* Template Name: Vídeos
* Description:
*
* @package audiotext
*/
global $post;
global $configuracao;
get_header(); ?>
	<!-- PAGINA DE VIDEOS -->
	<div class="pg pg-videos">
		<div class="container">
			<!-- TITULO VIDEO -->
			<h1 class="tituloVideos">Aprenda a transcrever áudio em texto e seja um excelente transcritor freelancer</h1>
			
			<!-- SESSAO DE VIDEOS -->
			<section class="sessaoVideos">
				<h6 class="hidden">Sessão de vídeos</h6>
				<!-- LISTA DE VIDEOS -->
				<ul class="listaDeVideos">
					<?php 
						$postVideos = new WP_Query(array(
							'post_type'     => 'videos',
							'posts_per_page'   => 1,
							)
						);

						while($postVideos->have_posts()) : $postVideos->the_post();
							$idVideo = rwmb_meta("Audiotext_id_video_LP");
							$descricaoVideo = rwmb_meta("Audiotext_descricao_lp_video");
					?>
					<li>
						<article>
							<!-- TITULO DO VIDEO -->
							<h2 class="tituloVideo"><?php echo get_the_title(); ?></h2>
							<figure>
								<!-- IMAGEM DO VIDEO -->
								<img src="http://i1.ytimg.com/vi/<?php echo $idVideo; ?>/hqdefault.jpg" alt="Thumbnail video">
							</figure>
							<!-- DESCRICAO DO VIDEO -->
							<p class="descriptionVideo"><?php echo $descricaoVideo ?></p>
							<!-- LINK DO VIDEO -->
							<a href="<?php echo get_permalink(); ?>" class="botaoVerVideo">ver vídeo</a>
						</article>
					</li>
					
					<?php endwhile; wp_reset_query(); ?>
				</ul>
			</section>
		</div>
	</div>

<?php get_footer(); ?>