﻿<?php
/**
* Template Name: Transcrição de Áudio em Texto
* Description:
*
* @package audiotext
*/
 global $post;
get_header();
?>

    <title>
        <?php echo get_the_title() ?>
    </title>

    <div class="pg pg-inicial">
        <section class="areaSobreEmpresa">
            <h6 class="hidden">Sobre a Audeiotext</h6>
            <div class="container containerConteudoFull">
                <div class="row">
                    <div class="col-sm-5">
                        <div class="areaTexto">
                            <?php echo $configuracao['opt_transcricao_quem_somos_texto'] ?>
                        </div>
                    </div>
                    <div class="col-sm-7">
                        <div class="areaCarrossel">
                            <div id="" class="carrosselDestaque">
                                <div class="areaVideo">
                                    <iframe src="https://player.vimeo.com/video/<?php echo $configuracao['opt_transcricao_quem_somos_video'] ?>" width="640" height="360" frameborder="0" allowfullscreen></iframe>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="carrosselClientesTopo">
            <h6 class="hidden">Carrossel de Clientes</h6>
            <ul>
                <li>
                    <span data-stop="<?php echo $configuracao['paginas_transcricao_valor1'] ?>" class="number">0</span>
                    <p><?php echo $configuracao['paginas_transcricao_texto_valor1'] ?></p>
                </li>
                <li>
                    <span data-stop="<?php echo $configuracao['paginas_transcricao_valor2'] ?>" class="number">0</span>
                    <p><?php echo $configuracao['paginas_transcricao_texto_valor2'] ?></p>
                </li>
                <li>
                    <span data-stop="<?php echo $configuracao['paginas_transcricao_valor3'] ?>" class="number">0</span>
                    <p><?php echo $configuracao['paginas_transcricao_texto_valor3'] ?></p>
                </li>
                <li>
                    <span data-stop="<?php echo $configuracao['paginas_transcricao_valor4'] ?>" class="number">0</span>
                    <p><?php echo $configuracao['paginas_transcricao_texto_valor4'] ?></p>
                </li>
            </ul>
            <div class="container">
                <div class="carrosselClientesPrincipal" id="carrosselClientesTopo">
                    <?php
          // LOOP COMO DEPOIMENTOS
          $postClientes = new WP_Query(
            array(
              'post_type'     => 'clientes',
              'posts_per_page'   => -1,
            )
          );
          while ( $postClientes->have_posts() ) : $postClientes->the_post();
            $logoCliente = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
            $logoCliente = $logoCliente[0];
            ?>
                        <div class="item">
                            <figure>
                                <img src="<?php echo $logoCliente ?>" alt="<?php echo get_the_title(); ?>">
                            </figure>
                        </div>
                        <?php endwhile; ?>
                </div>
            </div>
        </section>

        <section class="areaInfoServicos" style="display: none;">
            <h6 class="hidden">Informações serviços</h6>
            <div class="container">
                <ul>
                    <li>
                        <span data-stop="<?php echo $configuracao['paginas_inicial_info_audiotext_valor_1'] ?>" class="number"><?php echo $configuracao['paginas_inicial_info_audiotext_valor_1'] ?></span>
                        <strong><?php echo $configuracao['paginas_inicial_info_audiotext_titulo_1']  ?></strong>
                        <p>
                            <?php echo $configuracao['paginas_inicial_info_audiotext_desc_1'] ?>
                        </p>
                    </li>
                    <li>
                        <span data-stop="<?php echo $configuracao['paginas_inicial_info_audiotext_valor_2'] ?>" class="number"><?php echo $configuracao['paginas_inicial_info_audiotext_valor_2'] ?></span>
                        <strong><?php echo $configuracao['paginas_inicial_info_audiotext_titulo_2']  ?></strong>
                        <p>
                            <?php echo $configuracao['paginas_inicial_info_audiotext_desc_2'] ?>
                        </p>
                    </li>
                    <li>
                        <span data-stop="<?php echo $configuracao['paginas_inicial_info_audiotext_valor_3'] ?>" class="number"><?php echo $configuracao['paginas_inicial_info_audiotext_valor_3'] ?></span>
                        <strong><?php echo $configuracao['paginas_inicial_info_audiotext_titulo_3']  ?></strong>
                        <p>
                            <?php echo $configuracao['paginas_inicial_info_audiotext_desc_3'] ?>
                        </p>
                    </li>
                </ul>
            </div>
        </section>

        <?php if($configuracao['paginas_transcricao_descricao_titulo']): ?> 
        <section class="descriptionService">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="toQueTo">
                            <h2 class="topDescriptionService"><?php echo $configuracao['paginas_transcricao_descricao_titulo']; ?></h2>
                           
                            <h2 class="serviceTitle"><?php echo $configuracao['paginas_transcricao_descricao_subtitulo']; ?></h2>
                             
                            <?php echo $configuracao['paginas_transcricao_descricao_conteudo']; ?>
                            
                            <?php if($configuracao['paginas_transcricao_descricao_checkbox_botao'] == 0): ?>
                            
                            <a href="<?php echo $configuracao['paginas_transcricao_descricao_link_botao']; ?>" class=" buttonContent">
                                <?php echo $configuracao['paginas_transcricao_descricao_text_botao']; ?>
                            </a>
                            <?php else: ?>
                                <span class="abrirFormularioDeOrcamento buttonContent"><?php echo $configuracao['paginas_transcricao_descricao_text_botao']; ?></span>
                            <?php endif; ?>

                            <?php if($configuracao['paginas_transcricao_descricao_imagem_tabela']): ?>
                                <span class="abrirModalTabela buttonContent" data-tabela="<?php echo $configuracao['paginas_transcricao_descricao_imagem_tabela']['url']; ?>">Ver tabela comparativa</span>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="imageRightService">
                            <figure>
                                <img src="<?php echo $configuracao['paginas_transcricao_descricao_imagem']['url'] ?>" alt="Description Service">
                            </figure>
                        </div>
                    </div>                    
                </div>

            </div>
        </section>
        <?php endif; ?>

        <div>
            <span id="comofunciona" style="opacity:0;"> como funciona </span>
        </div>

        <section class="areaComoFunciona">
            <h6>Como funciona?</h6>
            <div class="container">
                <div class="row">
                    <?php
            // LOOP COMO FUNCIONA
            $postComoFunciona = new WP_Query(array(
                'post_type'     => 'como-funciona',
                'posts_per_page'   => -1,
                'tax_query'     => array(
                  array(
                    'taxonomy' => 'categoriacomoFunciona',
                    'field'    => 'slug',
                    'terms'    => 'pagina-transcricao-audio',
                  )
                )
              )
            );
            $i = 1;
            while ( $postComoFunciona->have_posts() ) : $postComoFunciona->the_post();
            ?>
                        <div class="col-sm-3">
                            <div class="iconeTexto">
                                <?php
              if ($urlIconeComoFunciona = rwmb_meta('Audiotext_iconeComoFunciona')):
                foreach ($urlIconeComoFunciona as $urlIconeComoFunciona):
                  $iconeComoFunciona = $urlIconeComoFunciona;
            ?>
                                    <img alt="<?php echo get_the_title() ?>" title="<?php echo get_the_title() ?>" src="<?php echo $iconeComoFunciona['full_url'] ?>" class="img-responsive">
                                    <?php endforeach;endif; ?>
                                    <span><b><?php echo $i ?> °</b><?php echo get_the_title() ?> </span>
                                    <p>
                                        <?php echo rwmb_meta('Audiotext_descricaoComoFunciona'); ?>
                                    </p>
                            </div>
                        </div>
                        <?php  $i++; endwhile; wp_reset_query();  ?>
                </div>
                <?php if ($configuracao['opt_inicial_como_funciona_btn']):?>
                
                <span class="button itemSolicitarOcamento">
                    <?php echo $configuracao['opt_inicial_quem_somos_btn'] ?>
                </span>
                <?php endif;?>
            </div>
        </section>

        <div>
            <span style="opacity:0;" id="clientes">areaClientes</span>
        </div>

        <section class="areaClientes">
            <h6>
                <?php echo $configuracao['paginas_inicial_logo_titulo'] ?>
            </h6>
            <div class="container">
                <button id="btncarrosselClientesLeft"><img src="<?php echo $configuracao['botao_carrosselEsquerda']['url'] ?>" alt="botão carrossel"></button>
                <button id="btncarrosselClientesRight"><img src="<?php echo $configuracao['botao_carrosselDireita']['url'] ?>" alt="botão carrossel"></button>
                <div class="carrosselClientes" id="carrosselClientes">
                    <?php
          // LOOP COMO DEPOIMENTOS
          $postClientes = new WP_Query(
            array(
              'post_type'     => 'clientes',
              'posts_per_page'   => -1,
            )
          );
          while ( $postClientes->have_posts() ) : $postClientes->the_post();
            $logoCliente = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
            $logoCliente = $logoCliente[0];

            ?>
                        <div class="item">
                            <figure>
                                <img src="<?php echo $logoCliente ?>" alt="<?php echo get_the_title(); ?>">
                            </figure>
                        </div>

                        <?php endwhile; ?>
                </div>
            </div>
        </section>

        <section class="areaDepoimentos">
            <h6 id="depoimentos">
                <?php echo $configuracao['paginas_inicial_depoimentos_titulo'] ?>
            </h6>
            <div class="container">
                <button id="btncarrosselDepoimentosLeft"><img src="<?php echo $configuracao['botao_carrosselEsquerda']['url'] ?>" alt="botão carrossel"></button>
                <button id="btncarrosselDepoimentosRight"><img src="<?php echo $configuracao['botao_carrosselDireita']['url'] ?>" alt="botão carrossel"></button>
                <div class="carrosselDepoimentos" id="carrosselDepoimentos">
                    <?php
          // LOOP COMO DEPOIMENTOS
          $postDepoimentos = new WP_Query(array(
            'post_type'     => 'depoimentos',
            'posts_per_page'   => -1,
            'tax_query'     => array(
                array(
                  'taxonomy' => 'categoriaDepoimentos',
                  'field'    => 'slug',
                  'terms'    => 'depoimentos-pagina-transcricao-de-audio',
                )
              )
            )
          );

                // LOOP DE DESTAQUE DA CATEGORIA MARCADA
                $i = 1;
                while ( $postDepoimentos->have_posts() ) : $postDepoimentos->the_post();
                  ?>
                                <div class="item">
                                    <i class="fa fa-quote-left"></i>
                            <?php
            if ($urlIconeComoFunciona = rwmb_meta('Audiotext_logoComoFunciona')):
              foreach ($urlIconeComoFunciona as $urlIconeComoFunciona):
                $logoDepoimentos = $urlIconeComoFunciona;
                ?>
                                <img alt="<?php echo get_the_title() ?>" title="<?php echo get_the_title() ?>" src="<?php echo $logoDepoimentos['full_url'] ?>" class="img-responsive">
                                <?php endforeach;endif; ?>
                                <p>
                                    <?php echo rwmb_meta('Audiotext_depoimento') ?>
                                    </span>
                        </div>
                        <?php  $i++; endwhile; wp_reset_query();  ?>
                </div>
            </div>
        </section>
        
        <?php if($configuracao['paginas_transcricao_descricao_titulo2']): ?>
        <section class="descriptionServiceTwo">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="imageRightService">
                            <figure>
                                <img src="<?php echo $configuracao['paginas_transcricao_descricao_imagem2']['url'] ?>" alt="Description Service">
                            </figure>
                        </div>
                    </div>    
                    <div class="col-md-6">
                        <div class="toQueTo">
                            <h2 class="topDescriptionService"><?php echo $configuracao['paginas_transcricao_descricao_titulo2'] ?></h2>
                            <h2 class="serviceTitle"><?php echo $configuracao['paginas_transcricao_descricao_subtitulo2'] ?></h2>
                           <?php echo $configuracao['paginas_transcricao_descricao_conteudo2'] ?>
                            
                            <?php if($configuracao['paginas_transcricao_descricao_checkbox_botao2'] == 0): ?>paginas_transcricao_descricao_text_botao2
                            <a href="<?php echo $configuracao['paginas_transcricao_descricao_link_botao2']; ?>" class=" buttonContent"> 
                                <?php echo $configuracao['paginas_transcricao_descricao_text_botao2']; ?>
                            </a>
                            <?php else: ?>
                                 <span class="abrirFormularioDeOrcamento buttonContent"><?php echo $configuracao['paginas_transcricao_descricao_text_botao2']; ?></span>
                            <?php endif; ?>

                            <?php if($configuracao['paginas_transcricao_descricao_imagem_tabela_2']): ?>
                                <span class="abrirModalTabela buttonContent" data-tabela="<?php echo $configuracao['paginas_transcricao_descricao_imagem_tabela_2']['url']; ?>">Ver tabela comparativa</span>
                            <?php endif; ?>
                        </div>
                        
                    </div>
                                    
                </div>

            </div>
        </section>
        <?php endif; ?>

        <div>
            <span id="porqueAudiotext" style="opacity:0; margin:10px 0; display:block;">Area Valores</span>
        </div>

        <section class="areaValores">
            <h2>
                <?php echo $configuracao['paginas_inicial_valores_audiotext_titulo'] ?>
            </h2>
            <div class="container">
                <ul>
                    <?php
				    $posts = new WP_Query( array(
					'post_type' => 'porque-confiar', 'orderby' => 'id','order' => 'asc','posts_per_page' => -1,'tax_query'     => array(
							array(
								'taxonomy' => 'categoriaConfiar',
								'field'    => 'slug',
								'terms'    => 'pagina-transcricao-de-audio',
							)
						)
					)
				);
                while ( $posts->have_posts() ) : $posts->the_post();
                  ?>
                                <li>
                            <i class="<?php echo rwmb_meta('Audiotext_iconeConfiar') ?>" aria-hidden="true"></i>
                            <h3><?php echo get_the_title() ?></h3>
                            <p>
                                <?php echo rwmb_meta('Audiotext_textoConfiar')  ?>
                            </p>
                        </li>
                        <?php endwhile; wp_reset_query(); ?>
                </ul>
            </div>
        </section>

        <?php if($configuracao['paginas_transcricao_descricao_titulo3']): ?>
        <section class="descriptionService serviceThree">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="toQueTo">
                            <h2 class="topDescriptionService"><?php echo $configuracao['paginas_transcricao_descricao_titulo3']; ?></h2>
                            <h2 class="serviceTitle"><?php echo $configuracao['paginas_transcricao_descricao_subtitulo3']; ?></h2>
                             <?php echo $configuracao['paginas_transcricao_descricao_conteudo3']; ?>

                             <?php if($configuracao['paginas_transcricao_descricao_checkbox_botao3'] == 0): ?>
                            <a href="<?php echo $configuracao['paginas_transcricao_descricao_link_botao3']; ?>" class=" buttonContent"><?php echo $configuracao['paginas_transcricao_descricao_text_botao3']; ?></a>
                            <?php else: ?>
                               <span class="abrirFormularioDeOrcamento buttonContent"><?php echo $configuracao['paginas_transcricao_descricao_text_botao3']; ?></span>
                            <?php endif; ?>

                            <?php if($configuracao['paginas_transcricao_descricao_imagem_tabela_3']): ?>
                                <span class="abrirModalTabela buttonContent" data-tabela="<?php echo $configuracao['paginas_transcricao_descricao_imagem_tabela_3']['url']; ?>">Ver tabela comparativa</span>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="imageRightService">
                            <figure>
                                <img src="<?php echo $configuracao['paginas_transcricao_descricao_imagem3']['url'] ?>" alt="Description Service">
                            </figure>
                        </div>
                    </div>                    
                </div>

            </div>
        </section>
        <?php endif; ?>

        <div><span id="servicos" style="opacity:0;"> servicos</span></div>

        <section class="areaServicos">
            <h2>
                <?php echo $configuracao['paginas_inicial_servicos_audiotext_titulo'] ?>
            </h2>
            <?php
			// RECUPERANDO CATEGORIAS
			$categoriaServicos = array(
				'taxonomy'     => 'categoriaServicos',
				'child_of'     => 0,
				'parent'       => 0,
				'orderby'      => 'description',
				'order'        => 'ASC',
				'pad_counts'   => 0,
				'hierarchical' => 1,
				'title_li'     => '',
				'hide_empty'   => 0
        			);
        			// SE HOUVER CATEGORIAS MOSTRAR ÁREA
        			if ($categoriaServicos):
        		?>
                <div class="container">
                    <div class="row">
                        <?php
						$listaCategoriasServicos = get_categories($categoriaServicos);

						// LISTANDO CATEGORIAS
						foreach ($listaCategoriasServicos as $listaCategoriasServicos):

						$postServicos = new WP_Query(array(
								'post_type'     => 'servicos',
								'posts_per_page'   => -1,
								'tax_query'     => array(
									array(
								'taxonomy' => $listaCategoriasServicos->taxonomy,
								'field'    => 'slug',
								'terms'    => $listaCategoriasServicos->slug,
										)
									)
								)
							);

							
					?>
                            <div class="col-md-4">
                                <button data-toggle="" data-target="#<?php echo $listaCategoriasServicos->slug; ?>">
							<div class="infoCategoriasServicos">
								<h2><?php echo $listaCategoriasServicos->name; ?> </h2>
								<p>
                                    <?php 
											if (function_exists('wp_get_terms_meta')){ 
											  $iconeCategoria = wp_get_terms_meta($listaCategoriasServicos->cat_ID, "icone-categoria" ,true); 
											} 

                                    ?>
                                    <?php echo $iconeCategoria; ?>
                                    <?php echo $listaCategoriasServicos->description ?></p>
							</div>
						</button>
                                <div class="areaPostsServicos collapse in" id="<?php echo $listaCategoriasServicos->slug; ?>">
                                    <?php
								while ( $postServicos->have_posts() ) : $postServicos->the_post();
						 ?>
                                        <div class="postServicos">
                                            <a class="abrirPostServicos" data-toggle="collapse" href="#<?php echo $post->ID ?>" aria-expanded="false">
                                                <h3 class="tituloPostServico">
                                                    <?php echo get_the_title(); ?>
                                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                                    <i class="fa fa-minus" aria-hidden="true"></i>
                                                </h3>
                                            </a>

                                            <?php 

											if (function_exists('wp_get_terms_meta')){ 
											  $corBotao = wp_get_terms_meta($listaCategoriasServicos->cat_ID, "cor-boatao" ,true); 
											 
											 
											} 

										?>
                                            <div class="collapse" id="<?php echo $post->ID ?>">
                                                <p>
                                                    <?php echo get_the_content(); ?>
                                                </p>
                                                <a href="<?php echo $linkPaginaServico = rwmb_meta('Audiotext_linkUrlServico');; ?>" style="background:<?php echo $corBotao; ?>" class=" saibaMais">saiba mais</a>
                                            </div>

                                        </div>

                                        <?php endwhile; wp_reset_query(); ?>
                                </div>
                            </div>
                            <?php endforeach; endif; ?>
                    </div>
                </div>
        </section>

        <div>
            <span id="duvidasFrequentes" style="opacity:0; margin:0 0 50px 0; display:block;">Duvidas Frequentes</span>
        </div>

        <section class="areaDuvidasFrequentes pg-faq">
            <h2>Dúvidas Frequentes</h2>
            <?php
                        // RECUPERANDO CATEGORIAS
                        $categoriaPerguntasFrequentes = array(
                          'taxonomy'     => 'categoriaFaq',
                          'child_of'     => 0,
                          'parent'       => 0,
                          'orderby'      => 'description',
                          'order'        => 'ASC',
                          'pad_counts'   => 0,
                          'hierarchical' => 1,
                          'title_li'     => '',
                          'hide_empty'   => 0
                        );
                        // SE HOUVER CATEGORIAS MOSTRAR ÁREA
                        if ($categoriaPerguntasFrequentes):

                      ?>
                                <div class="areaConteudo">
                                    <div class="row">
                                        <?php

                        $listaCategorias = get_categories($categoriaPerguntasFrequentes);

                        // LISTANDO CATEGORIAS
                        foreach ($listaCategorias as $listaCategorias):

                        // RECUPERANDO PERGUNTAS DE CATEGORIAS
                        $postsPerguntasFrequentes = new WP_Query(array(
                            'post_type'     => 'pergunta-frequentes',
                            'posts_per_page'   => -1,
                            'tax_query'     => array(
                              array(
                            'taxonomy' => $listaCategorias->taxonomy,
                            'field'    => 'slug',
                            'terms'    => $listaCategorias->slug,
                                )
                              )
                            )
                          );
                        ?>
                            <div class="col-sm-4">
                                <!-- ÁREA DE PERGUNTAS -->
                                <div class="areadePerguntas">

                                    <!-- TÍTULO DA CATEGORIA -->
                                    <h2><button data-toggle="collapse" data-target="#<?php echo $listaCategorias->slug; ?>"><?php echo $listaCategorias->name ?>  <i class="fas fa-sort-down"></i></button></h2>
                                    <div class="abrirSanfona collapse" id="<?php echo $listaCategorias->slug; ?>">


                                        <?php
              //LOOP DE PERGUNTAS
              while ( $postsPerguntasFrequentes->have_posts() ) : $postsPerguntasFrequentes->the_post();
              global $post;
              ?>
                                            <div class="conteudo">

                                                <!-- PERGUNTA  -->
                                                <a class="collapse" data-toggle="collapse" href="#<?php echo $post->ID ?>" aria-expanded="false">
                                                    <h3>
                                                        <?php echo get_the_title() ?>
                                                    </h3>
                                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                                    <i class="fa fa-minus" aria-hidden="true"></i>
                                                </a>

                                                <!-- RESPOSTA -->
                                                <div class="collapse" id="<?php echo $post->ID ?>">
                                                    <div class="card card-block">
                                                        <p>
                                                            <?php echo get_the_content() ?>
                                                        </p>
                                                    </div>
                                                </div>

                                            </div>
                                            <?php   endwhile; wp_reset_query();  ?>
                                    </div>
                                </div>
                            </div>
                            <?php endforeach; ?>

                    </div>
                </div>
                <?php endif; ?>
        </section>

        <div class="bannerpg" style="background:url(<?php echo $configuracao['opt_inicial_quem_somos_img_footer']['url'] ?>)"></div>
        <div class="areaQuemSomosfooter">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="areaTexto">
                            <h2><?php echo $configuracao['opt_inicial_quem_somos_titulo'] ?></h2>
                            <?php echo $configuracao['opt_inicial_quem_somos_texto_footer'] ?>
                            <?php if ($configuracao['opt_inicial_quem_somos_btn_footer']):?>
                            <span class="button itemSolicitarOcamento">
                                <?php echo $configuracao['opt_inicial_quem_somos_btn_footer'] ?>
                            </span>
                            <?php endif;?>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="divImagemQuemSomos">
                            <figure class="fotoEquipe">
                                <img src="<?php echo $configuracao['opt_inicial_quem_somos_imagem_equipe']['url'] ?>" alt="Foto da equipe da audiotext">
                            </figure>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="areaSejaumtexter">
            <h2>
                <?php echo $configuracao['opt_inicial_seja_um_texter'] ?>
            </h2>
           <span class="abrirModalEntreParaOTime">
                <?php echo $configuracao['opt_inicial_seja_um_texter_btn'] ?>
            </span>
        </div>
        <?php get_footer(); ?>
