<?php
/**
* Template Name: Vídeos
* Description:
*
* @package audiotext
*/
global $post;
global $configuracao;
get_header(); ?>
	<!-- PAGINA DE VIDEOS -->
	<div class="pg pg-videos">
		<div class="container">
			<!-- TITULO VIDEO -->
			<h1 class="tituloVideos">Aprenda a transcrever áudio em texto e seja um excelente transcritor freelancer</h1>
			
			<!-- SESSAO DE VIDEOS -->
			<section class="sessaoVideos">
				<h6 class="hidden">Sessão de vídeos</h6>
				<!-- LISTA DE VIDEOS -->
				<ul class="listaDeVideos">
					<?php 

						while(have_posts()) : the_post();
							$idVideo = rwmb_meta("Audiotext_id_video_LP");
							$descricaoVideo = rwmb_meta("Audiotext_descricao_lp_video");
					?>
					<li>
						<article>
							<a href="<?php echo get_permalink(); ?>">
								<!-- TITULO DO VIDEO -->
								<h2 class="tituloVideo"><?php echo get_the_title(); ?></h2>
								<figure>
									<!-- IMAGEM DO VIDEO -->
									<img src="http://i1.ytimg.com/vi/<?php echo $idVideo; ?>/hqdefault.jpg" alt="Thumbnail video">
								</figure>
								<!-- DESCRICAO DO VIDEO -->
								<p class="descriptionVideo"><?php echo $descricaoVideo ?></p>
							</a>
							<!-- LINK DO VIDEO -->
							<a href="<?php echo get_permalink(); ?>" class="botaoVerVideo">ver vídeo</a>
						</article>
					</li>
					
					<?php endwhile; wp_reset_query(); ?>
				</ul>
			</section>
		</div>
	</div>
	<div class="pg pg-inicial">
		<div class="areaSejaumtexter">
	        <h2>
	            <?php echo $configuracao['opt_inicial_seja_um_texter'] ?>
	        </h2>
	       <span class="abrirModalEntreParaOTime">
	            <?php echo $configuracao['opt_inicial_seja_um_texter_btn'] ?>
	        </span>
	    </div>
	</div>

<?php get_footer(); ?>